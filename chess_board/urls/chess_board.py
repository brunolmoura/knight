from django.urls import path

from chess_board.views import ChessBoardKnightAPIView


urlpatterns = [
    path(
        'chess_board/knight/<str:position>/',
        ChessBoardKnightAPIView.as_view(),
        name='chess-board'
    ),
]
