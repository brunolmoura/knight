from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from django.utils.translation import ugettext_lazy as _

from chess_board.utils import get_chess_board, get_knight_moves, \
    validate_position


class ChessBoardKnightAPIView(APIView):
    def get(self, request, position, format=None):
        invalid_postion_response = Response(
            {'detail': _("Invalid position, ex- E4, D6")},
            status=status.HTTP_400_BAD_REQUEST
        )

        if position is None:
            return Response(
                {'detail': _("'position' parameter is required")},
                status=status.HTTP_400_BAD_REQUEST
            )

        if validate_position(position) is False:
            return invalid_postion_response

        chess_board = get_chess_board()
        moves = get_knight_moves(position, chess_board, True)

        if not moves:
            return invalid_postion_response

        return Response(moves)
