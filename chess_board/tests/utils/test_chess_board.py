from django.test import TestCase

from chess_board.utils import get_chess_board, get_knight_moves, \
    validate_position


class ChessBoardTestCase(TestCase):

    def test_get_chess_board(self):
        chess_board = [
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1]
        ]
        assert chess_board == get_chess_board()

    def test_get_knight_moves(self):
        chess_board = get_chess_board()

        # Valid positions

        moves = get_knight_moves('A1', chess_board)
        assert moves == ["b3", "c2"]

        moves = get_knight_moves('A8', chess_board)
        assert moves == ["b6", "c7"]

        moves = get_knight_moves('D4', chess_board)
        assert moves == ["b3", "b5", "c2", "c6", "e2", "e6", "f3", "f5"]

        moves = get_knight_moves('H1', chess_board)
        assert moves == ["f2", "g3"]

        moves = get_knight_moves('H8', chess_board)
        assert moves == ["f7", "g6"]

        # Invalid positions

        moves = get_knight_moves('A0', chess_board)
        assert moves == []

        moves = get_knight_moves('A9', chess_board)
        assert moves == []

        moves = get_knight_moves('L1', chess_board)
        assert moves == []

        # Valid positions with next moves

        moves = get_knight_moves('A1', chess_board, True)
        next_moves = [
            {"b3": ["a1", "a5", "c1", "c5", "d2", "d4"]},
            {"c2": ["a1", "a3", "b4", "d4", "e1", "e3"]}
        ]
        assert moves == next_moves

        moves = get_knight_moves('A8', chess_board, True)
        next_moves = [
            {"b6": ["a4", "a8", "c4", "c8", "d5", "d7"]},
            {"c7": ["a6", "a8", "b5", "d5", "e6", "e8"]}
        ]
        assert moves == next_moves

        moves = get_knight_moves('D4', chess_board, True)
        next_moves = [
            {"b3": ["a1", "a5", "c1", "c5", "d2", "d4"]},
            {"b5": ["a3", "a7", "c3", "c7", "d4", "d6"]},
            {"c2": ["a1", "a3", "b4", "d4", "e1", "e3"]},
            {"c6": ["a5", "a7", "b4", "b8", "d4", "d8", "e5", "e7"]},
            {"e2": ["c1", "c3", "d4", "f4", "g1", "g3"]},
            {"e6": ["c5", "c7", "d4", "d8", "f4", "f8", "g5", "g7"]},
            {"f3": ["d2", "d4", "e1", "e5", "g1", "g5", "h2", "h4"]},
            {"f5": ["d4", "d6", "e3", "e7", "g3", "g7", "h4", "h6"]}
        ]
        assert moves == next_moves

        moves = get_knight_moves('H1', chess_board, True)
        next_moves = [
            {"f2": ["d1", "d3", "e4", "g4", "h1", "h3"]},
            {"g3": ["e2", "e4", "f1", "f5", "h1", "h5"]}
        ]
        assert moves == next_moves

        moves = get_knight_moves('H8', chess_board, True)
        next_moves = [
            {"f7": ["d6", "d8", "e5", "g5", "h6", "h8"]},
            {"g6": ["e5", "e7", "f4", "f8", "h4", "h8"]}
        ]
        assert moves == next_moves

    def test_validate_position(self):
        assert validate_position('a1') is True
        assert validate_position('a9') is True
        assert validate_position('aa') is False
        assert validate_position('1a') is False
        assert validate_position('1') is False
        assert validate_position('111') is False
