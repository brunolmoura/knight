from rest_framework import status
from rest_framework.test import APITestCase

from .helpers import ChessBoardAPI


class ChessBoardAPIViewTest(APITestCase):
    def setUp(self):
        self.chess_board_api = ChessBoardAPI()

    def test_get(self):
        # Valid position

        response = self.chess_board_api.get({'position': 'A1'})
        expected_response = [
            {"b3": ["a1", "a5", "c1", "c5", "d2", "d4"]},
            {"c2": ["a1", "a3", "b4", "d4", "e1", "e3"]}
        ]
        assert response.status_code == status.HTTP_200_OK
        assert response.data == expected_response

        # Invalid position
        response = self.chess_board_api.get({'position': 'AA'})
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data == {'detail': "Invalid position, ex- E4, D6"}

        response = self.chess_board_api.get({'position': '1A'})
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data == {'detail': "Invalid position, ex- E4, D6"}
