from common.helpers import KnightAPI


class ChessBoardAPI(KnightAPI):
    def __init__(self):
        KnightAPI.__init__(self)

    def get(self, params):
        url_name = 'chess-board'
        response = self.api_get(
            url_name,
            params
        )
        return response
