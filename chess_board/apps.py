from django.apps import AppConfig


class ChessboardConfig(AppConfig):
    name = 'chess_board'
