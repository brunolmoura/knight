import string


def get_chess_board():
    return [[1, 1, 1, 1, 1, 1, 1, 1] for i in range(8)]


def get_knight_moves(position, chess_board, next_moves=False):
    column, row = list(position.strip().lower())
    row = int(row) - 1
    column = string.ascii_lowercase.find(column)
    possible_moves = []

    if (row or column) > 7 or (row or column) < 0:
        return possible_moves

    moves = [
        (-2, -1),
        (-2, +1),
        (+2, -1),
        (+2, +1),
        (-1, -2),
        (-1, +2),
        (+1, -2),
        (+1, +2)
    ]

    for move in moves:
        try:
            move_row = row + move[0]
            move_column = column - move[1]
            if move_row >= 0 and move_column >= 0 and chess_board[move_row][move_column]:
                result = string.ascii_lowercase[move_column] + str(move_row + 1)
                possible_moves.append(result)
        except IndexError:
            pass

    possible_moves.sort()

    if next_moves:
        next_moves = []
        for move in possible_moves:
            next_moves.append({move: get_knight_moves(move, chess_board)})
        return next_moves

    return possible_moves


def validate_position(position):
    if len(position) != 2:
        return False

    column, row = list(position.strip().lower())

    if column.isalpha() is False:
        return False

    if row.isnumeric() is False:
        return False

    return True
