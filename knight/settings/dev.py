from .base import *


ALLOWED_HOSTS = ['*']

if DEBUG:
    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
        'silk.middleware.SilkyMiddleware',
    )

    DEBUG_APPS = (
        'debug_toolbar',
        'silk',
    )
    INSTALLED_APPS += DEBUG_APPS

    SILKY_PYTHON_PROFILER = True
    SILKY_META = True

    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': lambda request: True
    }
