from rest_framework.reverse import reverse
from rest_framework.test import APIClient


class KnightAPI(object):
    def __init__(self):
        self.client = APIClient()

    def api_get(self, url_name, params, api_format='json'):
        response = self.client.get(
            reverse(url_name, kwargs=params),
            format=api_format
        )
        return response
