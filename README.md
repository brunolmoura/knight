# knight

Given a knight located on a coordinate chosen by the user find out all possible locations
where the knight can move in 2 turns.

## Setup

On project root, do the following:

1. If you need to create your own local settings please create a copy of settings/dev.py
2. Create a copy of .env.example such as `cp .env.example .env`
3. Create a copy of docker-compose.db.yml.example such as `cp docker-compose.db.yml.example docker-compose.db.yml`
4. Create a copy of docker-compose.yml.example such as `cp docker-compose.yml.example docker-compose.yml`
5. You can customize all theses previous files as you like.
6. Start DB Server `sudo docker-compose -f docker-compose.db.yml up`. It's required to run step 7.
7. Start Application Server `sudo docker-compose up`.
8. Running tests, go into the container, go to project folder `cd /var/www/` and run tests `pytest`
8. Create a superuser, go into the container, go to project folder `cd /var/www/` and run `python manage.py createsuperuser`
9. Application will be running on `http://0.0.0.0:8000/` if default configuration was kept. 

## Work enviroment

 - PostgreSQL
 - Django 2+
 - Django Rest Framework
    - Django REST framework is a powerful and flexible toolkit for building Web APIs.
 - Django Rest Swagger
    - An API documentation generator for Swagger UI and Django REST Framework
 - Pytest
    - The pytest framework makes it easy to write small tests, yet scales to support complex functional 
    testing for applications and libraries.
 - Django Silk
    - Silk is a live profiling and inspection tool for the Django framework. Silk intercepts and stores HTTP requests 
    and database queries before presenting them in a user interface for further inspection 
 - Django Request
    - django-request is a statistics module for django. It stores requests in a database for admins to see, 
    it can also be used to get statistics on who is online etc. To find the request overview page, please click on 
    Requests inside the admin, then “Overview” on the top right, next to “add request”.
    
## API Documentation

Knight API documentation is self created with Django Rest Swagger and comes with a nice interface for to try it out. 

- `http://0.0.0.0:8000/docs/`

## Endpoint Reference

- `http://0.0.0.0:8000`


### Get all possible locations

- `GET /api/v1/chess_board/knight/H1/`


```json
[
    {
        "f2": [
            "d1",
            "d3",
            "e4",
            "g4",
            "h1",
            "h3"
        ]
    },
    {
        "g3": [
            "e2",
            "e4",
            "f1",
            "f5",
            "h1",
            "h5"
        ]
    }
]  
```

- `GET /api/v1/chess_board/knight/A8/`


```json
[
    {
        "b6": [
            "a4",
            "a8",
            "c4",
            "c8",
            "d5",
            "d7"
        ]
    },
    {
        "c7": [
            "a6",
            "a8",
            "b5",
            "d5",
            "e6",
            "e8"
        ]
    }
]   
```