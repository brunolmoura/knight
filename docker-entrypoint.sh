#!/bin/bash

rm /etc/localtime
ln -s /usr/share/zoneinfo/America/Recife /etc/localtime

cd /var/www/

pip install --upgrade pip
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver 0.0.0.0:$port
