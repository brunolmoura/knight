FROM ubuntu:16.04

MAINTAINER  "Chess Knight"

RUN apt-get update -y
RUN apt-get install wget -y
RUN apt-get install python3-pip -y
RUN apt-get install libpq-dev -y
RUN apt-get install libjpeg8-dev -y
RUN apt-get update \
    && apt-get install -y build-essential zlib1g-dev libssl-dev \
    && wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tgz \
    && tar -xvf Python-3.6.3.tgz \
    && cd Python-3.6.3 \
    && ./configure --with-zlib \
    && make && make altinstall \
    && cp /usr/local/bin/python3.6 /usr/local/bin/python \
    && cp /usr/local/bin/pip3.6 /usr/local/bin/pip

RUN apt-get install language-pack-en -y && apt-get install python3-dev -y

ADD knight/requirements.txt .
RUN pip3 install -r requirements.txt && rm requirements.txt

WORKDIR /var/www